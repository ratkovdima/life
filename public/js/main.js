Function.prototype.partial = function() {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function(){
        var arg = 0;
            var args_tmp = args.slice(0);
            for ( var i = 0; i < args.length && arg < arguments.length; i++ )
                if ( args_tmp[i] === undefined )
                    args_tmp[i] = arguments[arg++];
            return fn.apply(this, args_tmp);
     };
};

$.fn.serializeObject = function() {
    var o = {};
    $.each(this.serializeArray(), function(i, v) {
         o[v.name] = Number(v.value) ? parseInt(v.value, 10) : v.value;
    });
    return o;
}

require(['custom_form', 'sea', 'terminal'], function(custom_form, sea, terminal) {

});
