define([], function() {
var exist_storage = !!typeof(Storage);

if (exist_storage && !localStorage['lifeislife']) {
	localStorage.setItem("lifeislife", JSON.stringify({}));
}

function addHistoryCommand(command) {
	if (!exist_storage) {
		return;
	}
	var max_size = 100,
		storage_object = JSON.parse(localStorage.lifeislife);
	if (!storage_object['history_command']) {
		storage_object['history_command'] = [];
	}
	storage_object.history_command.push(command);
	if (storage_object.history_command.length > max_size) {
		storage_object.history_command = storage_object.history_command.slice(-1 * max_size);
	}
	localStorage.setItem("lifeislife", JSON.stringify(storage_object));
}

function getLastHistoryCommand(num, is_up) {
	if (!exist_storage) {
		return;
	}
	var storage_object = JSON.parse(localStorage.lifeislife),
		idx = num.idx;
	if (is_up) {
		idx++;
	} else {
		idx--;
	}
	if (!storage_object['history_command'] || idx > storage_object.history_command.length || idx <= 0) {
		if (idx <= 0) {
			num.idx = 0;
		}
		return;
	}
	if (is_up && idx == storage_object.history_command.length) {

	} else {
		num.idx = idx;
	}
	return storage_object.history_command[storage_object.history_command.length - idx];
}

return {
	addHistoryCommand: addHistoryCommand,
	getLastHistoryCommand: getLastHistoryCommand,
};

});