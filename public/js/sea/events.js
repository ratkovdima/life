define(['config'], function (config) {

function hover_cell() {
    $('[data-group="'+$(this).attr('data-group')+'"]')
        .css({'border-style': 'dotted', 'border-color': 'green', 'border-width': '1px', 'margin': '-1px'});
}

function unhover_cell() {
    $('[data-group="'+$(this).attr('data-group')+'"]')
        .css({'border-style': '', 'border-color': '', 'border-width': '', 'margin': ''});
}

return {
	hover_cell: hover_cell,
	unhover_cell: unhover_cell,
}

});