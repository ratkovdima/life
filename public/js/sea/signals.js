define(['config', 'sea/show_id'], function(config, show_number) {

function setGroupNextVenue(x, y) {
	var next_venue = x;
	if (y !== undefined && y !== 0) {
		next_venue = x + y * config.settings.venue_cols_count;
	}
	for (var group_id in show_number.getCellShowGroupID()) {
		show_number.hideTargetVenue(group_id);
		show_number.showTargetVenue(group_id, next_venue);
		config.setParamsCycle('group', {idx: group_id, next_venue: next_venue})
	}
}

return {
	setGroupNextVenue: setGroupNextVenue,
};

});