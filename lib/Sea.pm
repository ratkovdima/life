package Sea;
use Mojo::Base 'Mojo::EventEmitter';
use Mojo::Loader;
use Mojo::Base '-base';

use Cell;
use Engine;

use Data::Dumper;

has cells_free => sub { {} };
has cells_occupied => sub { [] };
has cells_children => sub { [] };
has cells_eaten => sub { [] };
has [qw(cols rows venue_cols_count venue_rows_count)];
has max_id => 0;
has max_idx => 0;
has destination_name => sub { [qw(left up right down)] };
has engine => sub {
    my $self = shift;
    Engine->new(
        width  => $self->venue_cols_count,
        height => $self->venue_rows_count
    )
};

has client => sub { {} };
has possible_entity => sub {
    [qw(Cell::Plankton Cell::Babelfish Cell::Nemo)]
};

sub new {
    my $class = shift;

    my $self = {};
    bless $self, $class;
    $self->client(shift);
    map { $self->$_($self->client->{$_}) } grep { $self->client->{$_} }
        qw(cols rows max_id venue_cols_count venue_rows_count);
    my $loader = Mojo::Loader->new;
    $loader->load($_) for (@{$self->possible_entity});
    # Very small sea, for test
    $self->venue_cols_count($self->cols)
        if $self->venue_cols_count > $self->cols; 
    $self->venue_rows_count($self->rows)
        if $self->venue_rows_count > $self->rows; 
    $self->engine->sea($self);
    $self->engine->groups($self->client->{'groups'})
        if $self->client->{'groups'};
    $self->cells_free({ 
        map { $_ => 1 } (0..$self->rows * $self->cols - 1)
    });
    return $self;
}

sub AddCell { return push @{shift->cells_occupied}, shift }

sub AddCellChildren { push @{shift->cells_children}, shift }

sub AddCellEaten { push @{shift->cells_eaten}, shift }

sub DelCellObject {
    my $self = shift;
    my $cell= shift;
    $self->cells_occupied(
        [ grep { $_->id != $cell->id }
            @{$self->cells_occupied} ]
    );

    $self->cells_children(
        [ grep { $_->id != $cell->id }
            @{$self->cells_children} ]
    );
}

sub DelCellEaten {
    my $self = shift;

    my @cells = @{$self->cells_occupied};
    foreach my $c (@cells) {
        if ($c->is_eaten) {
            $self->DelCellObject($c);
        }
    }
}

sub ObjFromClassName {
    my $self = shift;
    my $img = shift || return;

    for (@{$self->possible_entity}){
        return $_ if Cell::ShortName($_) eq [split '-', $img]->[0];
    }
}

sub FillingOneCell {
    my $self = shift;
    my $cell_class = shift;
    my $x = shift;
    my $y = shift;
    my $group = shift;

    unless ($self->_CheckOccupied($x, $y)){
        delete $self->cells_free->{$y * $self->cols + $x};
        my $added_cell = $cell_class->new(x => $x, y => $y, id => $self->_IncrMaxId(),
            group => $group, sea => $self
        );
        $self->AddCell($added_cell);
        return $added_cell;
    }
    return 0;
}

sub _IncrMaxId {
    my $self = shift;
    return $self->max_id($self->max_id + 1)->max_id;
}

sub _CheckOccupied {
    my $self = shift;
    my ($x, $y) = @_;

    return 1 if !defined $x || !defined $y;
    for (@{$self->cells_occupied}){
        return 1 if $_->x == $x && $_->y == $y;
    }
    for (@{$self->cells_children}){
        return 1 if $_->x == $x && $_->y == $y;
    }
    return 0;
}

sub _GetCellObject {
    my $self = shift;
    my ($x, $y) = @_;

    for (@{$self->cells_occupied}){
        return $_ if $_->x == $x && $_->y == $y;
    }
    return 0;
}

sub MergeCellsOccupied {
    my $self = shift;

    push @{$self->cells_occupied}, @{$self->cells_children}, @{$self->cells_eaten};
    my @merged;
    for (sort { $a->idx <=> $b->idx } (@{$self->cells_occupied})) {
        $_->idx(-1);
        push @merged, $_;
    }
    $self->cells_occupied(\@merged);
    $self->cells_children([]);
    $self->cells_eaten([]);
}

sub Run {
    my $self = shift;
    $self->max_idx(0);
    $self->DelCellEaten();
    my %cells = map { $_->id => $_ } @{$self->cells_occupied};
    my @rand_cells = map { delete $cells{[keys %cells]->[int rand keys %cells]} }
        keys %cells;
    foreach my $c (@rand_cells) {
        if ($c->is_die) {
            $self->cells_occupied(
                [ grep { $_->id != $c->id } @{$self->cells_occupied}]
            );
        } else {
            $c->setIdx();
            $c->Action($self);
        }
    }
    $self->MergeCellsOccupied();
}

1;

__END__

=pod

=encoding utf8
 
=head1 NAME
 
 Sea contains all cells 

=head1 SyNOPSIS

 use Sea;
 
 my $sea = Sea->new(
     rows => $rows_count, cols => $cols_count, max_id => $max_id,
     groups => $groups_info_json,
     venue_rows_count => $venue_rows_count,
     venue_cols_count => $venue_cols_count
 );
 $sea->AddCell($nemo);

=head1 ATTRIBUTES

=head2 cells_free

 my $free_cells_hash = $sea->cells_free;
 $sea->cells_free(
     { 
      map { $_ => 1 } 
     (0..$sea->rows * $sea->cols - 1)
     }
 );

 Contains all the free cells in sea by reference of hash.
 Keys is index of array cells of sea.

=head2 cells_occupied

 foreach my $cell (@{$sea->cells_occupied}){
     $cell->Action($sea);
 }
 
 push @{$sea->cells_occupied}, $cell;
 
 Contains reference of array with all the existing cell with entity.
 
=head2 cells_children

  push @{$sea->cells_children}, $cell;

 for (@{$sea->cells_children}){
     if($_->x == $x && $_->y == $y){
         return 1;
     }
 }

 Contains all new cell - children.

=head2 cells_occupied_all

 Merger of two variable arrays $sea->cells_children and $sea->cells_occupied
 
=head2 cols

 The number of cells in the sea by horizontally.
 
=head2 rows

 The number of cells on the sea by vertical.
 
=head2 venue_cols_count

 The number of cells on a venue by horizontally.

=head2 venue_rows_count

 The number of cells on a venue by vertical.
 
=head2 max_id

 The maximum id of cell's entity in the sea.
 
=head2 engine

 The instance of Engine.pm related with this sea.

=head2 possible_entity

 List of the possible entity in the sea.

=head1 METHODS

=head2 Run

 Starts the life cycle, after initialization 
 
=head2 AddCell

 Pushes cell to the array of occupieded cells.
 
=head2 AddCellChildren

 Pushes cell to the array of children's cell.
