package Cell::Plankton;
use Mojo::Base 'Cell';
use Mojo::Base '-base';

has class_skeleton => 'skeleton';

sub new {
    my $Class =  shift;
    my %Params = @_;

    my $self = $Class->SUPER::new(@_);

    return $self;
}

sub Action {
    my $self =  shift;

    $self->SUPER::Action(@_);
    return if $self->is_die || $self->is_eaten;
    if ($self->need_sex && $self->gender eq 'female'){
        $self->ActionPushChildren(); 
        $self->need_sex(0);   
    } else {
        $self->ActionMove();
    }
}

sub ActionPushChildren2 {
    my $self = shift;
        
    return 0 unless $self->SUPER::ActionPushChildren();
    $self->sea->DelCellObject($self->x, $self->y);
    $self->sea->AddCellChildren(
        $self->new(x => $self->x, y => $self->y,
                   id => $self->sea->_IncrMaxId(),
                   sea => $self->sea)); 
    #$self->is_die(1);
    return 1;
}

sub Init {
    my $class_name = shift;
    my $sea = shift;
    my $count = shift;

    for (@{$sea->possible_entity}){
        $sea->engine->FillingRand($_, $count) if $_ eq $class_name;
    }
}

1;
