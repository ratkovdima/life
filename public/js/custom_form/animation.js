define(['config', 'sea/utils', 'sea'], function (config, util, sea) {
settings = config.settings;

function doneFormSpecEffect(input, rate, status) {
    if (!input)
        return;
    var type = input.attr('name').substr(3);
    if (status === 'active') {
        sea.deleteCell(type);
        return;
    }
    if (input.attr('name') == 'ok_water') {
        setTimeout(function() {
                util.cleanSea();
            },
            settings.time_interval / rate * settings.rate_effect
        );
    } else {
        // TO DO
        util.load_form_data(settings);
        settings.part = type;
        sea.part();
    }
}

function formSpecEffect(open_block, block_2_open, input, status) {
    var rate = 10;
    if (!open_block) {
        block_2_open.prev().addClass('active');
        var block_2_open_width_part = block_2_open.outerWidth() / rate;
            for (var j = 1; j <= rate; j++) {
                setTimeout(function(j) {
                        block_2_open.css({display: 'inline-block', width: (block_2_open_width_part * j) + 'px'})
                    }, 
                    settings.time_interval / rate * j * settings.rate_effect,
                    j
                );
            }
        return;
    }
    var width = open_block.outerWidth(),
        width_part = width / rate;
    for (var i = 1; i <= rate; i++) {
        (function(i) {
            setTimeout(function(i) {
                open_block.css('width', (width - width_part * i) + 'px');  
                if (i == rate) {
                    open_block.hide().css('width', width);
                    block_2_open.prev().addClass('active');
                    var block_2_open_width_part = block_2_open.outerWidth() / rate;
                    for (var j = 1; j <= rate; j++) {
                        setTimeout(function(j) {
                                block_2_open.css({display: 'inline-block', width: (block_2_open_width_part * j) + 'px'});
                                if (j == rate) {
                                    doneFormSpecEffect(input, rate, status);
                                }
                            }, 
                            settings.time_interval / rate * j * settings.rate_effect,
                            j
                        );
                    }
                }
            },
            settings.time_interval / rate * i * settings.rate_effect,
            i)
        })(i)
    }    
}

function move_menu_logo(i, max_margin, logo, open_block, block_2_open) {
    setTimeout(function() {
        logo.css('margin-top', -i + 'px');
        if (i == max_margin) {
            for (var j = 1; j <= max_margin; j++) {
                setTimeout(function(j) {
                        logo.css('margin-top', -i + j + 'px');
                        if (j == max_margin)
                            formSpecEffect(open_block, block_2_open)
                    },
                    (settings.time_interval / 12 * j * settings.rate_effect),
                    j
                );
            }
        }
    }, 
    (settings.time_interval / 12 * i * settings.rate_effect),
    i
    );
}

return {
    doneFormSpecEffect: doneFormSpecEffect,
    formSpecEffect: formSpecEffect,
    move_menu_logo: move_menu_logo,
}

});
