package Cell;
use Mojo::Base '-base';

sub new {
    my $Class = shift;
    my %Params = @_;
    
    my $self = bless {}, ref $Class || $Class;
    map { $self->$_($Params{$_}) } grep { defined $Params{$_} }
        qw(x y id sea group need_sex is_die current_live_cycle gender need_food);
    $self->gender('male') if !$Params{gender} && int rand 2;
    warn $self->group;    
    $self->sea->engine->groups->{$self->group}->addMember($self);
    $self->LoadFormData();
    $self->setIdx();

    return $self;
}

has [qw(id x y last_x last_y move_img is_die sea group need_sex need_food
        has_children max_cycle_without_food max_live_cycle my_food eaten_id
        need_food_cycle reproduction_cycle class_skeleton class_eaten is_eaten)];

has current_live_cycle => 0;

has gender => 'female';
has around => sub { [] };
has idx => -1;
has max_density => 0.5;

has class_name => sub { shift->ClassNameCB() };


sub Action {
    my $self = shift;

    $self->last_x($self->x);
    $self->last_y($self->y);
    $self->class_eaten('');
    $self->eaten_id('');

    $self->current_live_cycle($self->current_live_cycle + 1);
    $self->need_food($self->need_food + 1) if defined $self->need_food;

    $self->need_sex(1)
        if $self->reproduction_cycle && $self->current_live_cycle > 1 &&
            $self->current_live_cycle % $self->reproduction_cycle == 0;
    $self->is_die(1)
        if ($self->max_cycle_without_food && $self->need_food == $self->max_cycle_without_food)
            || $self->max_live_cycle == $self->current_live_cycle;
    # TO DO fix warn $self->current_live_cycle;

    #Around random actions
    $self->around([]);
    push @{$self->around},
        {move => $self->can('_ActionMoveTopCell'),
         push_children => $self->can('_ActionPushChildTopCell'),
         food => $self->can('_ActionFoodTopCell'),
         x => $self->x, y => $self->y - 1}
            if $self->y;
    push @{$self->around},
        {move => $self->can('_ActionMoveRightCell'),
         push_children => $self->can('_ActionPushChildRightCell'),
         food => $self->can('_ActionFoodRightCell'),
         x => $self->x + 1, y => $self->y}
            if $self->x != $self->sea->cols - 1;
    push @{$self->around},
        {move => $self->can('_ActionMoveDownCell'),
         push_children => $self->can('_ActionPushChildDownCell'),
         food => $self->can('_ActionFoodDownCell'),
         x => $self->x, y => $self->y + 1}
            if $self->y != $self->sea->rows - 1;
    push @{$self->around},
        {move => $self->can('_ActionMoveLeftCell'),
         push_children => $self->can('_ActionPushChildLeftCell'),
         food => $self->can('_ActionFoodLeftCell'),
         x => $self->x - 1, y => $self->y}
            if $self->x;

}


sub ClassNameCB {
    my $self = shift;
    my $idx = shift;
    return sprintf(
        "%s-%s",
        $self->ShortName(),
        $self->sea->destination_name->[defined($idx) ? $idx : rand(4)]
    );
};

sub GetCellParams {
    my $self =  shift;
    my %res = map { $_ => $self->$_ }
        qw(id x y move_img last_x last_y is_die gender need_sex
            group class_name need_food current_live_cycle eaten_id
            max_live_cycle max_cycle_without_food class_eaten is_eaten
            reproduction_cycle need_food_cycle class_skeleton idx);
    return \%res;
}

sub GetDistance {
    my $self = shift;
    my ($cur_x, $cur_y, $target_x, $target_y) = @_;

    return abs($target_x - $cur_x) + abs($target_y - $cur_y); 
}

sub ShortName { return lc [split '::', ref $_[0] ? ref $_[0] : $_[0] ]->[-1] }

sub Move { $_[0]->x($_[1])->y($_[2]) }

sub setIdx {
    my $self =  shift;

    $self->idx($self->sea->max_idx);
    $self->sea->max_idx($self->sea->max_idx + 1);
}

sub LoadFormData {
    my $self = shift;

    #use Data::Dumper;
    #warn Dumper $self->sea->client;
    foreach my $field (qw(max_live_cycle max_cycle_without_food
                          need_food_cycle reproduction_cycle)) {
        my $count = $self->sea->client->{
            sprintf(
                "%s_%s", $self->ShortName(), $field
            )
        };
        next unless $count;
        my $delta = int(int(rand(20)) / 100 * $count);
        if (int rand 2) {
            $count += $delta;
        } else {
            $count -= $delta;
        }
        $self->$field($count);
    }
}

sub ActionPushChildren {
    my $self = shift;

    foreach (1..scalar @{$self->around}){
        if ($self->around->[int rand @{$self->around}]->{push_children}->($self)) {
            if (my $gr = $self->sea->engine->groups->{$self->group}) {
                if ($gr->engine->getVenueHeight() * $gr->engine->getVenueWidth() * $self->max_density * 2 <= $gr->getCount()) {
                    $gr->splitGroup();
                }
             }
            return 1;
        }
    }
    return 0;
}

sub ActionMove {
    my $self =  shift;
    
    return if $self->ActionMoveToVenue();

    for (1..scalar @{$self->around}){
        return 1
            if $self->around->[int rand @{$self->around}]->{move}->($self);
    }
}

sub ActionMoveToVenue {
    my $self = shift;
    return unless (defined $self->group);

    my $gr = $self->sea->engine->groups->{$self->group};
    my $next_venue_range = $gr->next_venue_range;
    my $target_x = $gr->target_x;
    my $target_y = $gr->target_y;

    my $cur_distance = $self->GetDistance($self->x, $self->y, $target_x, $target_y);
    my @around = 
        grep { $self->GetDistance($_->{x}, $_->{y}, $target_x, $target_y) < $cur_distance }
            @{$self->around};
    #warn scalar @around;
    @around = @{$self->around} unless @around;
    foreach (1..@around) {
        my $idx = int rand @around;
        return 1 if $around[$idx]->{move}->($self);
        @around = grep { $_ ne $around[$idx] } @around;
    }
    # Go but not back. If many cell stopped in venue
    # TO DO fix same next and current
    if ($gr->venue == $gr->next_venue) {
        @around = grep { $_->{y} != $self->y } @{$self->around};
        foreach (1..@around) {
            my $idx = int rand @around;
            return 1 if ($around[$idx]->{move}->($self));
            @around = grep { $_ ne $around[$idx] } @around;
        }
    }
    # TO DO if near from another group
    return 1;
}

sub ActionFood {
    my $self = shift;

    for (1..scalar @{$self->around}) {
        return 1 if $self->around->[int rand @{$self->around}]->{food}->($self);
    }
    return 0;
}

sub _ActionMoveTopCell {
    my $self = shift;
    
    return 0 unless $self->y;
    unless ($self->sea->_CheckOccupied($self->x, $self->y - 1)) {
        $self->last_y($self->y);
        $self->y($self->y - 1);
        $self->move_img('arrow-up');
        $self->class_name($self->ClassNameCB(1));
        return 1;
    }
    return 0;
}

sub _ActionMoveDownCell {
    my $self = shift;
    
    return 0 if $self->y == $self->sea->rows - 1;
    unless ($self->sea->_CheckOccupied($self->x, $self->y + 1)) {
        $self->last_y($self->y);
        $self->y($self->y + 1);
        $self->move_img('arrow-down');
        $self->class_name($self->ClassNameCB(3));
        return 1;
    }
    return 0;
}

sub _ActionMoveRightCell {
    my $self = shift;
    
    return 0 if $self->x == $self->sea->cols - 1;
    unless ($self->sea->_CheckOccupied($self->x + 1, $self->y)) {
        $self->last_x($self->x);
        $self->x($self->x + 1);
        $self->move_img('arrow-right');
        $self->class_name($self->ClassNameCB(2));
        return 1;
    }
    return 0;
}


sub _ActionMoveLeftCell {
    my $self = shift;
    
    return 0 unless $self->x;
    unless ($self->sea->_CheckOccupied($self->x - 1, $self->y)) {
        $self->last_x($self->x);
        $self->x($self->x - 1);
        $self->move_img('arrow-left');
        $self->class_name($self->ClassNameCB(0));
        return 1;
    }
    return 0;
}

sub _ActionPushChildTopCell {
    my $self = shift;
    
    return 0 unless $self->y;
    unless ($self->sea->_CheckOccupied($self->x, $self->y - 1)) {
        $self->sea->AddCellChildren(
            $self->new(x => $self->x, y => $self->y - 1,
                             id => $self->sea->_IncrMaxId(),
                             sea => $self->sea, group => $self->group));    
        return 1;
    }
    return 0;
}

sub _ActionPushChildDownCell {
    my $self = shift;
    
    return 0 if $self->y == $self->sea->rows - 1;
    unless ($self->sea->_CheckOccupied($self->x, $self->y + 1)) {
        $self->sea->AddCellChildren(
            $self->new(x => $self->x, y => $self->y + 1,
                             id => $self->sea->_IncrMaxId(),
                             sea => $self->sea, group => $self->group));    
        return 1;
    }
    return 0;
}

sub _ActionPushChildRightCell {
    my $self = shift;
    
    return 0 if $self->x == $self->sea->cols - 1;
    unless ($self->sea->_CheckOccupied($self->x + 1, $self->y)) {
        $self->sea->AddCellChildren(
            $self->new(x => $self->x + 1, y => $self->y,
                             id => $self->sea->_IncrMaxId(),
                             sea => $self->sea, group => $self->group));    
        return 1;
    }
    return 0;
}

sub _ActionPushChildLeftCell {
    my $self = shift;
    
    return 0 unless $self->x;
    unless ($self->sea->_CheckOccupied($self->x - 1, $self->y)) {
        $self->sea->AddCellChildren(
            $self->new(x => $self->x - 1, y => $self->y,
                             id => $self->sea->_IncrMaxId(),
                             sea => $self->sea, group => $self->group));    
        return 1;
    }
    return 0;
}

sub _ActionFoodTopCell {
    my $self = shift;
    
    return 0 unless $self->y;
    my $food = $self->sea->_GetCellObject($self->x, $self->y - 1);
    if ($self->CanEat($food)){
        $self->Eat($food);
        return 1;
    }
    return 0;
}

sub _ActionFoodDownCell {
    my $self = shift;
    
    return 0 if $self->y == $self->sea->rows - 1;
    my $food = $self->sea->_GetCellObject($self->x, $self->y + 1);
    if ($self->CanEat($food)){
        $self->Eat($food);
        return 1;
    }
    return 0;
}

sub _ActionFoodRightCell {
    my $self = shift;
    
    return 0 if $self->x == $self->sea->cols - 1;
    my $food = $self->sea->_GetCellObject($self->x + 1, $self->y);
    if ($self->CanEat($food)){
        $self->Eat($food);
        return 1;
    }
    return 0;
}


sub _ActionFoodLeftCell {
    my $self = shift;
    
    return 0 unless $self->x;
    my $food = $self->sea->_GetCellObject($self->x - 1, $self->y);
    if ($self->CanEat($food)){
        $self->Eat($food);
        return 1;
    }
    return 0;
}

sub CanEat {
    my $self = shift;
    my $food = shift || return 0;
    return 0 if $food->is_die;

    for (@{$self->my_food}) { return 1 if ref $food eq $_ }
    return 0;
}

sub getMaxDensityWithDelta {
    my $self = shift;
    my $percent = shift || 20;
    # with delta percent
    my $delta = int rand $self->max_density * $percent;
    return $self->max_density + $delta / 100 if int rand 2;
    return $self->max_density - $delta / 100;
}

sub Eat {
    my $self = shift;
    my $food = shift || return 0;

    $self->need_food(0);
    $self->class_eaten($food->class_name);
    $self->eaten_id($food->id);
    $food->is_eaten(1);
    $self->sea->DelCellObject($food);
    $self->sea->AddCellEaten($food) if $food->idx != -1 && $self->idx > $food->idx;
    if($self->x != $food->x){
        if($self->x > $food->x){
            $self->_ActionMoveLeftCell();
        } else {
            $self->_ActionMoveRightCell();
        }
    } else {
        if($self->y > $food->y){
            $self->_ActionMoveTopCell();
        } else {
            $self->_ActionMoveDownCell();
        }
    }
    return 1;

}

1;

__END__

=pod

=encoding utf8

=head1 NAME

Cell - base class of all entity in sea

=head1 SyNOPSIS

 package RealEntityCell;
 use Mojo::Base 'Cell';
 use Mojo::Base '-base';

=head1 DESCRIPTION

=over 1

=back

=head2 Package Variables

=over 2

=item id

 id of entity

=item x

 current position of coordinate 'x' in the sea

=item y

 current position of coordinate 'y' in the sea

=item last_x

 previous position of coordinate 'x' in the sea

=item  last_y

 previous position of coordinate 'y' in the sea

=item  move_img

 css class for arrow img

=item is_die

 flag that enitity is die

=item current_live_cycle

 integer value, is set in the child

=item max_live_cycle

 integer value, count cycle before die, is set in the child

=item reproduction_cycle

 integer value, cycle when time to reproduction, is set in the child

=item sea

 object of Sea.pm

=item gender

 male/female, is necessary to know for reproduction

=item need_sex

 store in memory that it's time to reproduction

=item has_children

 flag that ready to give birth

=item need_food

 flag that need food

=item group

 id of group, in which the entity
