define(['config', 'sea/utils', 'sea/animation', 'sea/events', 'history', 'history/sea_info', 'sea/show_id'],
    function(config, util, animation, events, history, history_sea, show_number) {

var settings = config.settings,
    first_cycle = 1,
    delay = setTimeout.partial(undefined, settings.time_interval, undefined),
    delay_init = setTimeout.partial(undefined, settings.time_interval / settings.rate_init, undefined),
    queue = [],
    SumTime = 0;

function delete_cell_class(attr_class) {
    return attr_class.replace('cell', '').replace('cell-24', '').replace(/\s+/g, ' ');
}

function cycle() {
    if (!first_cycle) {
        ws_sea.send(JSON.stringify(config.params_cycle()));
        config.delAllParamsCycle();
        return;
    }
    ws_sea.send(JSON.stringify(settings));
}

function part() {
    ws_sea.onmessage = onmessage;
    ws_sea.send(JSON.stringify(settings));
}

function queue_function(index, value, data) {
    return function(idx) {
    if (config.stop) {
        return;
    }
    show_number.hideNumber(value.id, 'only_erease');
    history.addCellInfo(value);
    var new_cell = util.getSeaCell(value.y * settings.cols + value.x)
        .children();

    var is_selected = false;
    if (util.getSeaCell(value.last_y * settings.cols + value.last_x).children().attr('class').indexOf('selected') != -1) {
        is_selected = true;
    }

    // First part condition fot init cycle
    var flag_move = !settings.part
        && (value.x != value.last_x || value.y != value.last_y);

    if (value.is_die) {
        history.addCell(new_cell.attr('class'), 'cell cell-24 ' + value.class_name, value.id);
        history_sea.actionSeaInfo(value.class_name, history_sea.addCellSkeleton);
    }
    if (flag_move){
        // Move
        var t = util.getSeaCell(value.last_y * settings.cols + value.last_x)
            .children();
                                        
        if (!value.is_die && value.move_img) {
            animation.specEffect(t, value.move_img,
                value.y > value.last_y, value.x > value.last_x,
                value.y == value.last_y, value.x == value.last_x);
        }
        t.replaceWith( util.CreateWaterCell() );
    }

    if (value.current_live_cycle == 0) {
        // New
        animation.specEffect(new_cell, 'plus');
        if (!settings.part) {
            history.addCell('cell cell-24 ' + value.class_name, 'cell cell-24 plus', value.id);
        }
        history_sea.actionSeaInfo(value.class_name, history_sea.newCellSeaInfo);
        history_sea.actionSeaInfo(value.class_name, history_sea.newCellPlus);
    } else if(value.class_eaten){
        // Eat
        animation.specEffect(new_cell, delete_cell_class(value.class_eaten));
        history.addCell(value.class_eaten, 'cell cell-24 food');
        history_sea.actionSeaInfo(value.class_eaten, history_sea.newCellFood);
        history_sea.actionSeaInfo(value.class_eaten, history_sea.deleteCellSeaInfo);
    }
    if (value.is_die) {
        history_sea.actionSeaInfo(value.class_name, history_sea.deleteCellSeaInfo);
        value.class_name = history.getCellInfo(value.id).class_skeleton;
    }
    if (!value.is_eaten || !flag_move) {
        new_cell.attr('class', '')
            .addClass('cell cell-24 ' + value.class_name)
            .attr({'data-group': value.group, 'data-id': value.id})
            .hover(events.hover_cell, events.unhover_cell)
            .click(history.showCellInfo);
    }
    if (!value.is_die && !value.is_eaten) {
        show_number.showNumber(value.id, value.group, value.current_live_cycle == 0 ? 'is_new' : false);
    } else {
        show_number.hideNumber(value.id);
    }
    if (is_selected) {
        new_cell.addClass('selected');
        history.showCellInfo(null, new_cell);
    }
            if (value.eaten_id) {
         //   alert(value.eaten_id)
        }
    history.delCellInfo(value);
    if (queue[++idx]) {
        if (first_cycle) {
            delay_init(queue[idx], idx);
        } else {
            if (config.pause) {
                pause(idx);
            } else {
                delay(queue[idx], idx);
            }
        }
    } else {
        if (data.cells.length && !settings.part)
            cycle();
    }

    // Last cycle
    if (data.cells.length - 1 == index) {
        $.each(data.cells, function(index, value) {
            if (!value.is_die)
                return;
            // Kill zombie
            var skeleton = util.getSeaCell(value.y * settings.cols + value.x)
                .children();
            animation.specEffect(skeleton, delete_cell_class(skeleton.attr('class')));
            skeleton.replaceWith( util.CreateWaterCell() );
        }); 
    }
}
}

function pause(idx) {
    setTimeout(function() {
        if (config.pause) {
            pause2(idx);
        } else {
            delay(queue[idx], idx);
        } 
    }, 2000);
}

function pause2(idx) {
    setTimeout(function() {
        if (config.pause) {
            pause(idx);
        } else {
            delay(queue[idx], idx);
        }
    }, 2000);
}

function deleteCell(type) {
    var rate = 10,
        cells = config.sea_div.find("."+type+"-left, "+"."+type+"-up, "+"."+type+"-right, "+"."+type+"-down");
    for (var i = 1; i <= rate; i++) {
        (function(i){
            setTimeout(function(){
                    cells.css('opacity', 1 - i / 10);
                    if (i == 10)
                        cells.replaceWith(util.CreateWaterCell());
                },
                (settings.time_interval / rate * i * settings.rate_effect)       
            );
        })(i)
    }
}

function onmessage(event) {
    var data = JSON.parse(event.data);
    if (data.res != 'ok')
        return
    SumTime = 0;
    queue = [];
    config.groups = data.groups;

    show_number.reloadTarget();

    $.each(data.cells, function(index, value) {
        // Only part with init cycle
        if (!settings.part || value.class_name.split('-')[0] == settings.part) {
            SumTime += first_cycle ? settings.time_interval / settings.rate_init : settings.time_interval;
            queue.push(queue_function(index, value, data));
        }
    });
    if (first_cycle) {
        delay_init(queue[0], 0);
    } else {
        delay(queue[0], 0);
    }
    first_cycle = 0;
}

function start() {
    delete settings.part;
    first_cycle = 0;
    cycle();
}

function stop() {
    config.stop = true;
    //config.sea_div.html('');
}

return {
    start: start,
    stop: stop,
    part: part,
    deleteCell: deleteCell,
}

});
