define(['sea/utils', 'storage', 'sea', 'sea/signals', 'sea/show_id'], function(sea_utils, storageHTML, sea, signals, show_number) {

var PS1 = "> ",
	last_command_idx = {idx: 0},
	terminal = $(".terminal").html(PS1),
	grid_commands = [
		{main_name: "hide grid", func: {cmd: sea_utils.hideGrid, aliases: ["убрать сетку"]}},
		{main_name: "show grid", func: {cmd: sea_utils.showGrid, aliases: ["показать сетку"]}},
	],
	show_number_id_commands = [
		{main_name: "show ids", func: {cmd: show_number.showAllID, aliases: ["показать идентификаторы"]}},
		{main_name: "hide ids", func: {cmd: show_number.hideIDs, aliases: ["скрыть идентификаторы"]}},
		{main_name: "unit", func: {cmd: show_number.showIDs, aliases: ["юнит"]}},
	],
	show_number_group_commands = [
		{main_name: "show groups", func: {cmd: show_number.showAllGroups, aliases: ["показать группы"]}},
		{main_name: "hide groups", func: {cmd: show_number.hideIDs, aliases: ["скрыть группы"]}},
		{main_name: "group", func: {cmd: show_number.showGroups, aliases: ["группа"]}},
	],
	start_stop_commands = [
		{main_name: "start", func: {cmd: sea.start, aliases: ["старт"]}},
		{main_name: "stop", func: {cmd: sea.stop, aliases: ["стоп"]}},
		{main_name: "pause", func: {cmd: sea_utils.pause, aliases: ["пауза"]}},
		{main_name: "go on", func: {cmd: sea_utils.goon, aliases: ["продолжить"]}},
	],
	target_commands = [
		{main_name: "set target", func: {cmd: signals.setGroupNextVenue, aliases: ["назначить цель"]}},
		{main_name: "hide target", func: {cmd: sea_utils.hideTargetVenue, aliases: ["скрыть цели"]}},
	],
	help_commands = [
		{main_name: "help", func: {cmd: toggleHelpBlock, aliases: ["помощь"]}},
	],
	commands = {},
	help_commands_block = $('.terminal-commands'),
	terminal_block = $('.terminal-block');

_allCommands2Hash(grid_commands);
_allCommands2Hash(show_number_id_commands);
_allCommands2Hash(show_number_group_commands);
_allCommands2Hash(start_stop_commands);
_allCommands2Hash(target_commands);
_allCommands2Hash(help_commands);

$('.terminal-help').click(function() {
	toggleHelpBlock();
});

terminal_block.draggable();
terminal.keyup(function(event) { 
	if (event.which == 13) {
		newCommand();
	} else if (event.which == 38 || event.which == 40) {
		var last_command = storageHTML.getLastHistoryCommand(last_command_idx, event.which == 38 ? true : false),
			val_arr = terminal.val().split("\n");
		if (!last_command) {
			last_command = '';
		}
		val_arr[val_arr.length - 1] = PS1 + last_command;
		terminal.val(val_arr.join("\n"));
	}
});

terminal.keydown(function(e) {
	if (e.which == 38 || e.which == 40) {
		return false;
	}
	if (e.which == 13) {
		var temp_arr= terminal.val().split("\n");
    	terminal.val('');
    	terminal.focus();
    	// only last 4 command in textarea
    	terminal.val(temp_arr.slice(-4).join("\n"));
	}
	if (e.which == 37 || e.which == 8) {
		var cursor_position = terminal.prop('selectionStart');
		if (terminal.val().substr(cursor_position - PS1.length, PS1.length) === PS1) {
			return false;
		}
	}
});

terminal.focus(function() { $(this).attr('rows', 5) })
	.blur(function() {
		var $this = $(this);
		setTimeout(function() { $this.attr('rows', 1).scrollTop(9999999) }, 300)
	});

function fillHelp() {
	_drowHelp(grid_commands);
	_drowHelp(show_number_id_commands);
	_drowHelp(show_number_group_commands);
	_drowHelp(start_stop_commands);
	_drowHelp(target_commands);
}
fillHelp();

function toggleHelpBlock() {
	if (help_commands_block.hasClass('hide')) {
		help_commands_block.removeClass('hide');
	} else {
		help_commands_block.addClass('hide');
	}
}

function _allCommands2Hash(cmnds) {
	$.each(cmnds, function(i, v) {
		commands[v.main_name] = v.func.cmd;
		$.each(v.func.aliases, function(i, val){
			commands[val] = v.func.cmd;
		});
	});
}

function _drowHelp(cmnds) {
	var div = $('<div>');
	$.each(cmnds, function(i, v) {
		var span = $('<span>').append($('<input>').attr({type: 'radio', name: 't_c'}));
		span.append($('<span>').html(v.main_name));
		if (v.func.aliases.length) {
			span.append($('<span>').html(' ( ' + v.func.aliases.join(', ') + ' )'))
		}
		(function(cmd) {
			span.click(cmd);
		})(v.func.cmd);
		div.append(span.addClass('cmnds'));
	});
	help_commands_block.append(div);
}

function newCommand() {
	var val = terminal.val(),
		last_command = val.split("\n").slice(-2)[0];
	terminal.val(val + PS1);
	if (last_command.indexOf(PS1) != 0) {
		return;
	}
	last_command = last_command.replace(PS1, '');
	last_command.trim();
	last_command = last_command.toLowerCase();
	if (last_command) {
		storageHTML.addHistoryCommand(last_command);
	}
	var num_arr = last_command.match(/\d+/g),
		command_without_num = last_command.replace(/\s+\d+/g, '');

	if (commands[last_command]) {
		commands[last_command]();
	} else if (commands[command_without_num] && num_arr) {
		$.each(num_arr, function(i) { num_arr[i] = parseInt(num_arr[i], 10) });
		commands[command_without_num].apply(this, num_arr);
	}
}

});