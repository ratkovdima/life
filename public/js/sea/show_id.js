define(['config'], function(config) {
var show_cell_number = '',
	show_cell_number_id = {},
	current_group = -1,
	show_group_number_id = {};

function _isShowID() { return show_cell_number === 'id' ? true : false }

function _isShowGroup() { return show_cell_number === 'group' ? true : false }

function _setShowID() { show_cell_number = 'id' }

function _setShowGroup() { show_cell_number = 'group' }

function _setShowNothing() { show_cell_number = '' }

function _addCellShowID(id) { show_cell_number_id[id] = 1 }

function _deleteCellShowID(id) { delete show_cell_number_id[id] }

function _addCellShowGroupID(group_id) { show_group_number_id[group_id] = 1 }

function _deleteCellShowGroupID(group_id) { delete show_group_number_id[group_id] }

function getCellShowGroupID() { return $.extend({}, show_group_number_id) }

function _hideAll() { hideIDs(); hideGroupIDs() }

function _showNumber(id, group_id) {
	if (!_isShowGroup() && !_isShowID()) {
		return;
	}
	var cell = $('i[data-id=' + id + ']');
	if (!cell.size()) {
		return;
	}
	_addCellShowID(id);
    $('<i>').html(_isShowID() ? id : group_id)
        .attr('data-show-id', id)
        .addClass(_isShowID() ? 'show-id' : 'show-groups').insertAfter(cell);
}

function _hideNumber(id, only_erease) { 
	if (!only_erease) {
		_deleteCellShowID(id);
	}
	$('i[data-show-id=' + id + ']').remove()
}

function _showGroup(group_id) {
    $.each($('i[data-group=' + group_id + ']'), function(i, v) {
        _showNumber($(v).attr('data-id'), group_id);
    });
}

function _hideGroup(group_id) {
	_deleteCellShowGroupID(group_id);
    $.each($('i[data-group=' + group_id + ']'), function(i, v) {
        _hideNumber($(v).attr('data-id'));
    });	
}

function showGroups() {
	_hideAll();
	if (!_isShowGroup()) {
		_setShowGroup();
	}
	for (var i = 0; i < arguments.length; i++) {
		var group_id = arguments[i];
		_showGroup(group_id);
		_addCellShowGroupID(group_id);
		showTargetVenue(group_id);
	}
}

function showIDs() {
	_hideAll();
	if (!_isShowID()) {
		_setShowID();
	}
	for (var i = 0; i < arguments.length; i++) {
		_showNumber(arguments[i]);
	}
}

function showNumber(id, group_id, is_new) {
	if (is_new) {
		_showNumber(id, group_id, is_new);
		return;		
	}
	for (var key in show_cell_number_id) {
		if (key == id) {
			_showNumber(id, group_id)
			break;
		}
	}
}

function hideNumber(id, only_erease) { _hideNumber(id, only_erease) }

function hideIDs() {
	for (var id in show_cell_number_id) {
		_hideNumber(id);
	}
	for (var id in show_cell_number_id) {
		_setShowNothing();
		break;
	}
}

function hideGroupIDs() {
	for (var id in show_group_number_id) {
		_hideGroup(id);
		hideTargetVenue(id);
	}
	for (var id in show_group_number_id) {
		_setShowNothing();
		break;
	}
}

function showTargetVenue(group_id, target_id) {
	if (target_id == undefined && config.groups[group_id]) {
		target_id = config.groups[group_id].next_venue
	}
    if (config.groups[group_id]) {
        $('.group[group=' + target_id + ']').addClass('target').attr('data-target-group-id', group_id);
    } else {
    	_deleteCellShowGroupID(group_id);
    }
}

function hideTargetVenue(group_id) {
    $('.group.target[data-target-group-id=' + group_id + ']').removeClass('target').removeAttr('data-target-group-id')
}

function reloadTarget() {
	for (var group_id in show_group_number_id) {
		hideTargetVenue(group_id);
		showTargetVenue(group_id);
	}
}

function showAllGroups() {
	_hideAll();
	_setShowGroup();
	$.each($('i.cell[data-id]'), function(i, v) {
		_showNumber($(v).attr('data-id'), $(v).attr('data-group'));
	});
}

function showAllID() {
	_hideAll();
	_setShowID();
	$.each($('i.cell[data-id]'), function(i, v) {
		_showNumber($(v).attr('data-id'));
	});
}

return {
	showGroups: showGroups,
	showIDs: showIDs,
	hideIDs: hideIDs,
	showNumber: showNumber,
	hideNumber: hideNumber,
	reloadTarget: reloadTarget,
	getCellShowGroupID: getCellShowGroupID,
	hideTargetVenue: hideTargetVenue,
	showTargetVenue: showTargetVenue,
	showAllGroups: showAllGroups,
	showAllID: showAllID,
}

});