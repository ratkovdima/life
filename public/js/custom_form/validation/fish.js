define(['config', './utils', 'sea/utils'], function (config, utils, sea_utils) {

var form = config.form_customize.get(0);

function validation(type) {
	var count = form[type + '_count'],
		max_live_cycle = form[type + '_max_live_cycle'],
		max_cycle_without_food = form[type + '_max_cycle_without_food'],
		need_food_cycle = form[type + '_need_food_cycle'],
		reproduction_cycle = form[type + '_reproduction_cycle'];

	utils.clearValidationMethod();

	utils.addMethod(utils.checkInteger, [count]);
	utils.addMethod(utils.checkInteger, [max_live_cycle]);
	utils.addMethod(utils.checkInteger, [max_cycle_without_food]);
	utils.addMethod(utils.checkInteger, [need_food_cycle]);
	utils.addMethod(utils.checkInteger, [reproduction_cycle]);

	var cell_free = sea_utils.getFreeCellCount();
	utils.addMethod(utils.checkMax, [count, cell_free, 'Свободных ячеек осталось ' + cell_free]);
	utils.addMethod(utils.checkMax, [max_cycle_without_food, max_live_cycle.value]);
	utils.addMethod(utils.checkMax, [need_food_cycle, max_live_cycle.value]);
	utils.addMethod(utils.checkMax, [reproduction_cycle, max_live_cycle.value]);

	return utils.applyMethod();
}

return {
	validation: validation,
}

});