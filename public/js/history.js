define([], function() {

var history_block = $('#history'),
	cell_info = {};

function addCell(cell_class, action_class, id) {
	var span = $('<span>').attr('data-id', id).append($('<i>').addClass(cell_class))
		.append($('<i>').addClass(action_class));
	//history_block.prepend(span);
}


function addCellInfo(cell) {
	if (cell_info[cell.id]) {
		cell_info[cell.id].current_live_cycle = cell.current_live_cycle;
		cell_info[cell.id].need_food = cell.need_food;
		cell_info[cell.id].class_name = cell.class_name;
		cell_info[cell.id].class_eaten = cell.class_eaten;
	} else {
		cell_info[cell.id] = cell;
	}
}

function delCellInfo(cell) {
	if (cell.is_die) {
		delete cell_info[cell.id];
		return;
	}
	if (cell.eaten_id) {
		delete cell_info[cell.eaten_id];
		return;
	}
}

function clearHistoryBlock() {
	history_block.html('');
}

function showCellInfo(event, el) {
	if (!el) {
		el = $(this);
		$('i.cell.selected').removeClass('selected');
	}
	var c = cell_info[el.addClass('selected').attr('data-id')],
		info = $('<div>');
	clearHistoryBlock();
	if (!c) {
		return;
	}
	info.append(paramName('Время жизни:'))
		.append(paramValue(c.max_live_cycle))
		.append(paramName('Текущий цикл жизни:'))
		.append(paramValue(c.current_live_cycle))
		.append(paramName('Цикл размножения:'))
		.append(paramValue(c.reproduction_cycle))
		.append(paramName('Пол:'))
		.append(paramValue(c.gender === 'male' ? 'М' : 'Ж'));
	if (c.need_food_cycle) {
		info.append(paramName('Цикл еды:'))
			.append(paramValue(c.need_food_cycle))
			.append(paramName('Циклов без еды:'))
			.append(paramValue(c.max_cycle_without_food))
			.append(paramName('Без еды уже:'))
			.append(paramValue(c.need_food));
	}
	history_block.append(info);
}

function paramValue(value) {
	return $('<span>').addClass('val').html(value);
}

function paramName(txt) {
	return $('<span>').addClass('name').html(txt);
}

function getCellInfo(id) { return cell_info[id] }

return {
	addCell: addCell,
	addCellInfo:addCellInfo,
	showCellInfo: showCellInfo,
	getCellInfo: getCellInfo,
	delCellInfo: delCellInfo,
}

});
