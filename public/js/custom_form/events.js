define(['custom_form/utils', 'custom_form/animation', 'config'],
    function (form_utils, animation, config) {

var form = config.form_customize.get(0);

function checkboxSaveParams(el) {
    var parent = _getContainerByCheckbox(el),
        logo = parent.find('.cell-logo');
    logo.addClass('ok');
    parent.find('.ok-small').removeClass('hide');
    form_utils.hideCellParamsEmpty(_getNextLogo(parent));
    form_utils.showCellParamsEmpty(logo);
    animation.formSpecEffect($(parent.find('.cell-param').get(1)),
        _getNextParams(parent),
        $(el),
        'inactive'
    );
}

function checkboxDeleteParams(el) {
    var parent = _getContainerByCheckbox(el),
        logo = parent.find('.cell-logo');
    logo.removeClass('ok active');
    parent.find('.ok-small').addClass('hide');
    form_utils.hideCellParamsEmpty(_getNextLogo(parent));
    form_utils.showCellParamsEmpty(logo);
    animation.formSpecEffect($(parent.find('.cell-param').get(1)),
        _getNextParams(parent),
        $(el),
        'active'
    );
}

var block_customize_height;

function clickStart() {
    block_customize_height = $('#customize').height();
    var step = block_customize_height / 10;
    for (var i = 1; i <= 10; i++) {
        (function(i) {
            setTimeout(function() {
                $('#customize').height((block_customize_height - step * i) + 'px');
            }, settings.time_interval / 12 * i * settings.rate_effect);
        })(i);
    }
    require(['sea'], function(sea) {
        sea.start();
    });
    $(this).addClass('hidden');
    $(".cell-stop").removeClass('hidden');
}

function clickStop() {
    var step = block_customize_height / 10;
    for (var i = 1; i <= 10; i++) {
        (function(i) {
            setTimeout(function() {
                $('#customize').height((step * i) + 'px');
            }, settings.time_interval / 12 * i * settings.rate_effect);
        })(i);
    }
    require(['sea'], function(sea) {
        sea.start();
    });
    $(this).addClass('hidden');
    $(".cell-go").removeClass('hidden');
    require(['sea'], function(sea) {
        sea.stop();
    });
}

function clickLogo(el) {
    if (!el) {
        el = this
    }
    var open_block,
        $this = $(el),
        block_2_open = $this.prev(),
        max_margin = 7;
    $.each(config.cell_logo, function(index, value) {
        if ($(value).prev().css('display') == 'inline-block') {
            open_block = $(value).prev();
            form_utils.showCellParamsEmpty($(value));
        }
    });
    if (open_block) {
        open_block.prev().removeClass('active');
        form_utils.hideCellParamsEmpty($this);
    }
    for (var i = 1; i <= max_margin; i++) {
        animation.move_menu_logo(i, max_margin, $this, open_block, block_2_open);
    }
}

function simpleIncrementInput(event, el) {
    if (!el) {
        el = $(this);
    }
    var input = el.parent().find('input'),
        val = -1,
        is_incr = false;
    if (el.hasClass('arrow-input-up')) {
        val = 1;
        is_incr = true;
    }
    if (_incrementColsRows(input, is_incr)) {
        return;
    }
    input.val(val + parseInt(input.val(), 10));
}

function increment10Input() {
    var input = $(this).parent().find('input');
    if ($(this).hasClass('t')) {
        input.val(parseInt(input.val(), 10) + 10);
    } else {
        input.val(input.val() - 10);
    }
}

function keyDownInput(event) {
    if ( event.which != 38 && event.which != 40 )
        return;
    var input = $(this).parent().find('input');
    if ( event.which == 38 ) {
        simpleIncrementInput(null, input.parent().find('.arrow-input-up'));
    } else {
        simpleIncrementInput(null, input.parent().find('.arrow-input-down'));
    }
}

function _incrementColsRows(input, is_incr) {
    var name = input.attr('name'),
        val = 0,
        k = -1,
        value = parseInt(input.val(), 10);
    if (name === 'cols' || name === 'rows') {
        if (name === 'cols') {
           val = form.venue_cols_count.value;
        } else if (name === 'rows') {
            val = form.venue_rows_count.value;
        }
        if (is_incr) {
            k = 1;   
        }
        for (var i = 0; i < val; i++) {
            if ((value + i * k) % val == 0) {
                if (i) {
                    val = i;
                }
                break;
            }
        }
        input.val((val * k) + value);
        return true;
    }
    return false;
}

function _existNextParams(el) {
    return !!el.next().find('.cell-logo')
}

function _getNextLogo(el) {
    return _existNextParams(el) ? 
        el.next().find('.cell-logo') :
        el.prev().find('.cell-logo')
}

function _getNextParams(el) {
    return _existNextParams(el) ?
        $(el.next().find('.cell-param').get(1)) :
        $(el.prev().find('.cell-param').get(1))
}

function _getContainerByCheckbox(el) {
    return $(el).parent().parent().parent().parent();
}

return {
    checkboxSaveParams: checkboxSaveParams,
    checkboxDeleteParams: checkboxDeleteParams,
    clickLogo: clickLogo,
    clickStart: clickStart,
    clickStop: clickStop,
    simpleIncrementInput: simpleIncrementInput,
    increment10Input: increment10Input,
    keyDownInput: keyDownInput,
}

});