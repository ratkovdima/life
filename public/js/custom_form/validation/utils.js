define(['config', 'utils'], function (config, utils) {

var el_tooltip,
	validation_method = [];

function addMethod(f, p) {
	validation_method.push({f:f, p:p});
}

function applyMethod() {
	for (var i = 0; i < validation_method.length; i++) {
		if (!validation_method[i].f.apply( this, validation_method[i].p )) {
			return false;
		}
	}
	return true;
}

function clearValidationMethod() {
	validation_method = [];
}

function destroyTooltip() {
	if (!el_tooltip) {
		return;
	}
	el_tooltip.tooltip('destroy');
	return el_tooltip;
}

function showTooltip(title, el) {
	if (!(el instanceof jQuery)) {
		el = $(el)
	}
	el_tooltip = el;
	destroyTooltip().tooltip({placement: 'bottom', title: title}).tooltip('show');
}

function checkInteger(el) {
	if (!utils.isInt(el.value) || el.value <= 0) {
		showTooltip('Должно быть целое число больше нуля', el);
		return false;
	}
	return true;
}

function checkMax(el, max, title) {
	max = parseInt(max, 10);
	if (el.value > max) {
		if (!title) {
			title = 'Здесь должно быть меньше ' + (max + 1);
		}
		showTooltip(title, el);
		return false;
	}
	return true;
}

function checkMin(el, min, title) {
	min = parseInt(min, 10);
	if (el.value < min) {
		if (!title) {
			title = 'Здесь должно быть больше ' + (min - 1);
		}
		showTooltip(title, el);
		return false;
	}
	return true;
}

function checkMultiple(el, multiple, title) {
	multiple = parseInt(multiple, 10);
	if (el.value % multiple) {
	 	if (!title) {
			title = 'Должно быть кратно ' + multiple;
		}
		showTooltip(title, el);
		return false;
	}
	return true;
}

return {
	showTooltip: showTooltip,
	destroyTooltip: destroyTooltip,
	checkInteger: checkInteger,
	checkMax: checkMax,
	checkMin: checkMin,
	addMethod: addMethod,
	applyMethod: applyMethod,
	clearValidationMethod: clearValidationMethod,
	checkMultiple: checkMultiple,
}

});