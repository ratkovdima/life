package Life::Main;
use Mojo::Base 'Mojolicious::Controller';

use Cell;
use Cell::Plankton;
use Cell::Babelfish;
use Mojo::JSON;
use Mojo::IOLoop;
use Mojo::Log;
use Sea;
use Data::Dumper;

sub index {
    my $self = shift;

    $self->render();
}

my $clients = {};

sub life {
    my $self = shift;
    my $tx = $self->tx;
    my $cid = "$tx";
    my $client = $tx;
    $clients->{$cid} = {};

    Mojo::IOLoop->stream($self->tx->connection)->timeout(3000);
    
    $self->on(message => \&cycle);
    
    $self->on(finish => sub {
         my ($self, $code, $reason) = @_;
         delete $clients->{$cid};
    });
}

sub cycle {
    my ($self, $message) = @_;
    my $client = $clients->{sprintf("%s", $self->tx)};
    my $json = Mojo::JSON->new;
    my $log = Mojo::Log->new(path => '/home/dim/perl/test/life/log/mojo.log', level => 'warn');
    #warn Dumper $log;
    my $data = $json->decode($message);
    my $part = $data->{part};
    my $sea = $client->{sea} ||= Sea->new($data);

    if ($data) {
        if (defined $data->{group} && defined $data->{group}->{idx}) {
            warn $data->{group}->{next_venue};
            $sea->engine->groups->{$data->{group}->{idx}} = 
                $sea->engine->groups->{$data->{group}->{idx}}->GetNextVenue($data->{group}->{next_venue});
        }
    }
    if ($part) {
        map { $_->Init($sea, $data->{Cell::ShortName($_) . '_count'}) } 
        grep { !$part || Cell::ShortName($_) eq $part }
            @{$sea->possible_entity};
        $sea->MergeCellsOccupied();
    } else {
        $sea->Run();
    } 
    $sea->engine->GetGroupsWithNextVenue();
    $client->{'cells'} = [
        map { $_->GetCellParams() } @{$sea->cells_occupied}
    ];
    %{$client->{'groups'}} = 
        map { $_ => $sea->engine->groups->{$_}->ToHash() } keys %{$sea->engine->groups};
    #warn Dumper $sea->engine->groups;
    #warn Dumper $client->{groups};
    #warn Dumper $client->{cells};
    #$log->warn(Dumper $client->{cells});
    #warn Dumper($log);
    #warn scalar  @{$sea->cells_occupied};
    my $json_res = { res => 'ok',
        map { $_ => $client->{$_} } qw(cells groups),
    };
    $self->send($json->encode($json_res));
}

1;
