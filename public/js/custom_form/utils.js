define([], function() {

function hideCellParamsEmpty(logo) {
    getCellParamsEmptyByLogo(logo).hide();
}

function getCellParamsEmptyByLogo(logo) {
    return logo.parent().find('.cell-param-empty');
}

function showCellParamsEmpty(logo) {
    getCellParamsEmptyByLogo(logo).css('display', 'inline-block');
}

return {
	hideCellParamsEmpty: hideCellParamsEmpty,
	showCellParamsEmpty: showCellParamsEmpty
}

});