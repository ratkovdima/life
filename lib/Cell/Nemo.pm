package Cell::Nemo;
use Mojo::Base 'Cell::Fish';
use Mojo::Base '-base';

has max_density => 0.35;

sub Init {
    my $class_name = shift;
    my $sea = shift;
    my $count = shift;

    for (@{$sea->possible_entity}){
        $sea->engine->Filling($_, $count, $sea->engine->can('RouteAll'))
            if $_ eq $class_name;
    }
}


1;
