define(['config'], function (config) {
    var settings = config.settings;

function specEffect(cell, class_name, is_plus_y, is_plus_x, is_move_y, is_move_x) {
    if (config.stop) {
        return;
    }
    var img = $('<i>')
        .addClass('cell cell-24 ' + class_name)
        .css({
            top: cell.position() ? cell.position().top : 0,
            left: cell.position() ? cell.position().left : 0,
            position: 'absolute',
            'z-index': 1000, opacity: 1
        });
    $('body').append(img);
    for (var i = 1; i <= 10; i++) {
        _specEffectTimer(i, img, is_move_x, is_move_y, is_plus_x, is_plus_y);
    }
    setTimeout(function(){ img.remove(); }, (settings.time_interval * settings.rate_effect));
}

function _specEffectTimer(i, img, is_move_x, is_move_y, is_plus_x, is_plus_y) {
    setTimeout(function() {
            img.css({opacity: 1 - i / 10,
                top: img.position().top + (is_move_y ? 0 : (img.height() / 10) * (is_plus_y ? 1 :-1)),
                left: img.position().left + (is_move_x ? 0 : (img.height() / 10) * (is_plus_x ? 1 : -1))    
            }) 
        },
        (settings.time_interval / 10 * i * settings.rate_effect)
    );
}

return {
    specEffect: specEffect,
}

});
