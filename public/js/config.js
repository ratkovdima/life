define([], function() {

var params_cycle = {};

function setParamsCycle(key, value) {
    params_cycle[key] = value;
}

function delAllParamsCycle() { params_cycle = {} }

function getParamsCycle() { return params_cycle }

return {
    form_customize: $('form[name=customize]'),
    sea_div: $('#sea'),
    cell_logo: $(".cell-logo"),
    stop: false,
    params_cycle: getParamsCycle,
    setParamsCycle: setParamsCycle,
    delAllParamsCycle: delAllParamsCycle,
    settings: {
        img_path: '/img/cell/',
        size: '24x24',
        arrow_id: 'arrow_',
        time_interval: 100,
        rate_effect: 5,
        rate_init: 3,
        direction_img: {up: '_up', right: '_right', down: '_down', left: '_left'},
    }
}
});
