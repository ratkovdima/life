define(['config', './utils', 'sea/utils'], function (config, utils, sea_utils) {

var form = config.form_customize.get(0);

function validation() {
	var count = form.plankton_count,
		max_live_cycle = form.plankton_max_live_cycle,
		reproduction_cycle = form.plankton_reproduction_cycle;

	utils.clearValidationMethod();

	utils.addMethod(utils.checkInteger, [count]);
	utils.addMethod(utils.checkInteger, [max_live_cycle]);
	utils.addMethod(utils.checkInteger, [reproduction_cycle]);

	var cell_free = sea_utils.getFreeCellCount();
	utils.addMethod(utils.checkMax, [count, cell_free, 'Свободных ячеек осталось ' + cell_free]);
	utils.addMethod(utils.checkMax, [reproduction_cycle, max_live_cycle.value]);

	return utils.applyMethod();
}

return {
	validation: validation,
}

});