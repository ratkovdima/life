define(['config', 'sea/utils', 'sea', 'custom_form/utils', 'custom_form/events', 'custom_form/animation', 'custom_form/validation'],
	function(config, util, sea, form_utils, events, animation, validation) {

var form = config.form_customize.get(0),
    settings = config.settings;

function InitForm() {
    var logo = $(config.cell_logo.get(0));
    logo.addClass('active').prev().css('display', 'inline-block');
    form_utils.hideCellParamsEmpty(logo);
}

InitForm();

settings = util.load_form_data(settings);

config.cell_logo.click(validation.clickLogo);

$(".cell-go").click(events.clickStart);

$(".cell-stop").click(events.clickStop);

config.form_customize.find('input[type=checkbox].save-params').click(function() {
    if (this.checked)
   	   validation.checkboxSaveParams(this);
    else  
        events.checkboxDeleteParams(this)       
});

$('.arrow-input-up, .arrow-input-down').click(events.simpleIncrementInput);

$('.rewind-wrap').click(events.increment10Input);

config.form_customize.find('input').keydown(events.keyDownInput);
	
});
