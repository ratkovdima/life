define(['config', './validation/water', './validation/utils', "./events", './validation/fish', './validation/plankton'],
	function (config, water, utils, events, fish, plankton) {

var form = config.form_customize.get(0);

function clickLogo() {
	utils.destroyTooltip();
	var water_logo = config.cell_logo.get(0);
	if (!form.ok_water.checked && this != water_logo) {
		utils.showTooltip('Сначала необходимо указать параметры акватории', water_logo);
	} else {
		events.clickLogo(this);
	}
}

function checkboxSaveParams(el) {
	var $this = $(el),
		name_checkbox = $this.attr('name');

	utils.destroyTooltip();
	el.checked = false;
	if (name_checkbox.indexOf('water') != -1) {
		if(!water.validation()) {
			return;
		}
	} else if (name_checkbox.indexOf('plankton') != -1) {
		if(!plankton.validation()) {
			return;
		}
	} else {
		if(!fish.validation(name_checkbox.split('_')[1])) {
			return;
		}
	}
	el.checked = true;
	events.checkboxSaveParams(el);
}

return {
	clickLogo: clickLogo,
	checkboxSaveParams: checkboxSaveParams,
}

});