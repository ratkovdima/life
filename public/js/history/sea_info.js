define([], function () {

var sea_info_block = $('#sea-info'),
	sea_info_hash = {},
	plus = {}
	food = {},
	skeleton = {};

function actionSeaInfo(cell_class, callback) {
	var possible = ['babelfish', 'nemo', 'plankton'];
	for (var i = 0; possible.length; i++) {
		if (cell_class.indexOf(possible[i]) != -1) {
			callback(possible[i]);
			return;
		}
	}
}

function newCellSeaInfo(cell_name) {
	if (sea_info_hash[cell_name]) {
		changeBlockSeaInfo(cell_name);
	} else {
		addBlockSeaInfo(cell_name);
	}
}

function deleteCellSeaInfo(cell_name) {
	if (sea_info_hash[cell_name] == 1) {
		removeBlockSeaInfo(cell_name);
	} else {
		changeBlockSeaInfo(cell_name, 'remove');
	}
}

function removeBlockSeaInfo(cell_name) {
	sea_info_block.find('.' + cell_name + '.all').remove();
}

function addBlockPlus(cell_name) {
	sea_info_block.append(createActionCell(cell_name, 'plus'));
	plus[cell_name] = 1;
}

function changeBlockPlus(cell_name) {
	plus[cell_name]++;
	sea_info_block.find('.' + cell_name + '.plus > .count').html('&nbsp;x&nbsp;'+plus[cell_name]);
}

function addSkeleton(cell_name) {
	sea_info_block.append(createActionCell(cell_name, 'skeleton'));
	skeleton[cell_name] = 1;
}

function changeSkeleton(cell_name) {
	skeleton[cell_name]++;
	sea_info_block.find('.' + cell_name + '.skeleton > .count').html('&nbsp;x&nbsp;'+skeleton[cell_name]);
}

function addCellSkeleton(cell_name) {
	if (skeleton[cell_name]) {
		changeSkeleton(cell_name);
	} else {
		addSkeleton(cell_name);
	}
}

function addBlockFood(cell_name) {
	sea_info_block.append(createActionCell(cell_name, 'food'));
	food[cell_name] = 1;
}

function changeBlockFood(cell_name) {
	food[cell_name]++;
	sea_info_block.find('.' + cell_name + '.food > .count').html('&nbsp;x&nbsp;'+food[cell_name]);
}

function newCellPlus(cell_name) {
	if (plus[cell_name]) {
		changeBlockPlus(cell_name);
	} else {
		addBlockPlus(cell_name);
	}
}

function newCellFood(cell_name) {
	if (food[cell_name]) {
		changeBlockFood(cell_name);
	} else {
		addBlockFood(cell_name);
	}
}

function addBlockSeaInfo(cell_class) {
	var block = $('<div>').addClass(cell_class + ' all').append(
		$("<div>").addClass('cell-24')
			.append($('<i>').addClass('cell cell-24 ' + cell_class + '-left'))
		).append($('<div>').addClass('count').html('&nbsp;x&nbsp;1'));
	sea_info_hash[cell_class] = 1;
	sea_info_block.append(block);
}

function createActionCell(cell_class, action_class) {
	return $('<div>').addClass(cell_class + ' ' + action_class).append(
		$('<div>').addClass('cell-24').append($('<i>').addClass(cell_class + '-left cell cell-24'))
			.append($('<i>').addClass(action_class + ' cell cell-24'))
	).append($('<div>').addClass('count').html('&nbsp;x&nbsp;1'));
}

function changeBlockSeaInfo(cell_class, remove_cell) {
	var s = sea_info_hash;
	if (remove_cell) {
		s[cell_class]--
	} else {
		s[cell_class]++
	}
	sea_info_block.find('.' + cell_class + '.all > .count').html('&nbsp;x&nbsp;'+s[cell_class]);
}

return {
	actionSeaInfo: actionSeaInfo,
	newCellSeaInfo: newCellSeaInfo,
	deleteCellSeaInfo: deleteCellSeaInfo,
	newCellPlus: newCellPlus,
	newCellFood: newCellFood,
	addCellSkeleton: addCellSkeleton,
}

})