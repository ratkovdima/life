define([], function() {

function isInt(n) {
    return n % 1 === 0;
}

return {
    isInt: isInt,
}

});
