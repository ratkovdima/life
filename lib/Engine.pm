package Engine;

use Data::Dumper;
use Mojo::Base '-base';

use Group;

# venue counts
has [qw(width height sea)];

has max_school_width => 4;
has max_school_height => 4;

has venue_free => sub { {} };
# TO DO for init new

has step_density => 0.2;

has routes => sub { [] };

has groups => sub { {} };

sub new {
    my $class = shift;
    my %Params = @_;

    my $self = {};

    bless $self, $class;
    $self->$_($Params{$_}) for qw(width height);

    push @{$self->routes}, $self->can($_)
        for qw(RouteHexagon RouteEnvelope);
    
    return $self->venue_free(
        { map { $_ => 1 } (0..$self->width * $self->height - 1) }
    );
}

sub GetVenuePositions {
    my $range = shift->GetVenueRange(shift);
    my @positions = ();
    for (my $i = $range->[0]->[0]; $i <= $range->[1]->[0]; $i++){
        for (my $j = $range->[0]->[1]; $j <= $range->[1]->[1]; $j++){
            push @positions, [$i, $j];
        }
    }
    return \@positions;
}

sub getVenueHeight { my $self =  shift; return int $self->sea->rows / $self->height }

sub getVenueWidth { my $self = shift; return int $self->sea->cols / $self->width }

sub GetVenueRange {
    my $self = shift;
    my $pos = shift;
    my $x = $pos % $self->width;
    my $y = int $pos / $self->width;

    my $venue_height = $self->getVenueHeight();
    my $venue_width = $self->getVenueWidth();
    return [
        [$x * $venue_width, $y * $venue_height],
        [$x == $self->width - 1 ? $self->sea->cols - 1 : $x * $venue_width + $venue_width - 1,
         $y == $self->height - 1 ? $self->sea->rows - 1 : $y * $venue_height + $venue_height - 1]
    ];
}

sub GetGroupsWithNextVenue {
    my $self = shift;
    my $groups = $self->groups;

    foreach my $gr (keys %$groups) {
        $groups->{$gr}->all(0)->inner(0);
    }

    foreach (map { $_->GetCellParams() } grep { defined $_->{group} } @{$self->sea->cells_occupied}){
        my $gr = $_->{group};
        $groups->{$gr} = {} unless $groups->{$gr};
        my $next_venue_range = $self->groups->{$gr}->next_venue_range;
        $groups->{$gr}->inner($groups->{$gr}->inner + 1)
            if $_->{x} >= $next_venue_range->[0]->[0] &&
               $_->{x} <= $next_venue_range->[1]->[0] &&
               $_->{y} >= $next_venue_range->[0]->[1] &&
               $_->{y} <= $next_venue_range->[1]->[1];
        $groups->{$gr}->all($groups->{$gr}->all + 1);
    }
    foreach (keys %$groups) {
        my $gr = $self->groups->{$_};
        if ($gr->time_wait) {
            # Time to wait
            if ($gr->time_wait == 4) {
                $gr->GetNextVenue();
                next;
            }
            $gr->time_wait($gr->time_wait + 1);
        }
        else {
            $gr->time_wait(1)
                if $groups->{$_}->all &&
                    $groups->{$_}->inner &&
                    $groups->{$_}->inner / $groups->{$_}->all >= 0.1; # min count
        }
        $gr->GetNextVenue()
            if $groups->{$_}->all && $groups->{$_}->inner &&
                $groups->{$_}->inner / $groups->{$_}->all >= $groups->{$_}->getMaxDensity(); # max count
    }
    # TO DO branch big group

    return $self->groups;
}

sub Filling {
    my $self = shift;
    my $cell_class = shift;
    my $count = shift || return;
    my $route = shift;

=c
    # TO DO
    #$possible_venue = $route_positions unless scalar @$possible_venue;
    unless (scalar @$possible_venue) {
        $self->max_density($self->max_density + $self->step_density);
        $possible_venue = $route_positions
            = $self->routes->[int rand scalar @{$self->routes}]->($self)
    }
    return unless scalar @$possible_venue;
=cut
    my $gr = Group->new(engine => $self, route => $route);
    my $venue_positions = $gr->venue_positions;
    my $venue_positions_count = scalar @$venue_positions;
    delete $self->venue_free->{$gr->venue};
    foreach (1..$count) {
        # TO DO venue is empty
        my $rand_pos = $venue_positions->[int rand @$venue_positions];
        unless ($rand_pos) {
            $self->FillingRand($cell_class, $count - $gr->getCount());
            return;
        }
        $venue_positions = [
            grep {$_->[1] * $self->width + $_->[0] != 
                  $rand_pos->[1] * $self->width + $rand_pos->[0]
                 } @$venue_positions
        ];
        my $added_cell = $self->sea->FillingOneCell(
            $cell_class,
            $rand_pos->[0],
            $rand_pos->[1],
            (scalar keys %{$self->groups}) - 1
        );
        redo unless $added_cell;
        $gr->setMaxDensity($added_cell->getMaxDensityWithDelta())
            unless $gr->getMaxDensity();
        # If venue filled more then max density then find next free venue
        return $self->Filling($cell_class, $count - $gr->getCount(), $route)
            if ($gr->getCount() / $venue_positions_count) > $gr->getMaxDensity();
    }
    return 1;
}

sub FillingRand {
    my $self = shift;
    my $class = shift;
    my $count = shift || return;

    my $sea = $self->sea;
    foreach (1..$count) {
        my $idx = [keys %{$sea->cells_free}]->[int rand keys %{$sea->cells_free}];
        $sea->FillingOneCell($class, $idx % $sea->cols, int $idx / $sea->cols);
    }
}

sub RouteAll {
    my $self = shift;

    return [0..$self->height * $self->width - 1]
}

1;
