define(['config'], function(config) {

var settings = config.settings;

function load_form_data(s) {
    s = $.extend(s, config.form_customize.serializeObject());
    return s;
}

function getCurrentGroup() { return current_group }
    
function cleanSea() {
    load_form_data(settings);
    config.sea_div.html('');
    var cell_hash = {};
    var idx = 0,
        group_width = parseInt(settings.cols / settings.venue_cols_count, 10),
        group_height = parseInt(settings.rows / settings.venue_rows_count, 10),
        rows_tmp = 0,
        venue_rows_cnt = 1;
    $.each(new Array(settings.rows), function(row) {
        var class_group_rows = '';
        if ((venue_rows_cnt != settings.venue_rows_count && rows_tmp == group_height - 1))  {
            class_group_rows = 'group-rows';
            rows_tmp = 0;
            venue_rows_cnt++;
        } else {
            rows_tmp++;
        }
        var span = $('<span>').addClass('group ' + class_group_rows).attr('group', getGroupByCoordinate(0, row, group_width, group_height)),
            cols_tmp = 0,
            venue_cols_cnt = 1;
        config.sea_div.append($('<div>'));
        $.each(new Array(settings.cols), function(cols) {
            var cell = $('<div>')
                .addClass('invisible cell-24 ')
                .append(CreateWaterCell());
            span.append(cell);
            cell_hash[idx++] = 1;
            if ((venue_cols_cnt != settings.venue_cols_count && cols_tmp == group_width - 1) || cols == settings.cols - 1)  {
                config.sea_div.append(span);
                span = $('<span>').addClass('group ' + class_group_rows).attr('group', getGroupByCoordinate(cols + 1, row, group_width, group_height));
                cols_tmp = 0;
                venue_cols_cnt++;
            } else {
                cols_tmp++;
            }
        });
    });
    $.each(new Array(settings.rows), function(row){
        $.each(new Array(settings.cols), function(cols){
            var arr = [];
            for (k in cell_hash) 
                arr.push(k);
            var rand_idx = arr[parseInt(Math.random() * arr.length, 10)];
            setTimeout(function() {
                getSeaCell(rand_idx).removeClass('invisible') },
                (settings.time_interval / 12 * row * settings.rate_effect)
            );
            delete cell_hash[rand_idx];
        });
    });

    $('.group').hover(
        function() {
            $('.group[group=' + $(this).attr('group') + ']').css('background-color', '#FCCCCC');
        },
        function() {
            $('.group[group=' + $(this).attr('group') + ']').css('background-color', '');
        }
    );
    showGrid();
}

function getSeaCell(idx) {
    return $(config.sea_div.find('div.cell-24').get(idx))
}

function CreateWaterCell() { 
    return $('<i>').addClass('cell-24 cell water')
}

function getGroupByCoordinate(x, y, group_width, group_height) {
    return parseInt(y / group_height, 10) * settings.venue_cols_count + parseInt(x / group_width, 10);
}

function getFilledCellCount() {
    var type = ['nemo_count', 'babelfish_count', 'plankton_count'],
        count = 0;
    $.each(type, function(i) {
        if (settings['ok_' + type[i]]) {
            count += settings[type[i]];
        }
    });
    return count;
}

function showGrid() {
    $(".group").css({'border-left': 'solid 1px red', 'border-left-style': 'dashed'});
    $(".group-rows").css({'border-bottom': 'solid 1px red', 'border-bottom-style': 'dashed'});
}

function hideGrid() {
    $(".group").css({'border-left': '', 'border-left-style': ''});
    $(".group-rows").css({'border-bottom': '', 'border-bottom-style': ''});
}

function getFreeCellCount() {
    return settings.cols * settings.rows - getFilledCellCount()
}

function pause() { config.pause = true }

function goon() { config.pause = false }

return {
    load_form_data: load_form_data,
    cleanSea: cleanSea,
    CreateWaterCell: CreateWaterCell,
    getSeaCell: getSeaCell,
    getFilledCellCount: getFilledCellCount,
    getFreeCellCount: getFreeCellCount,
    showGrid: showGrid,
    hideGrid: hideGrid,
    pause: pause,
    goon: goon,
    getCurrentGroup: getCurrentGroup,
}

});
