package Group;
use Mojo::Base '-base';
use Data::Dumper;

has [qw(route_positions venue next_venue_range next_venue venue_idx
		possible_venue target_x target_y venue_positions engine
		time_wait route id max_density)];
has all => 0;
has inner => 0;
has count_cell => 0;
has member => sub { {} };

sub new {
	my $class = shift;
	my %params = @_;

	my $self = bless {}, ref $class ? ref $class : $class;

	for (qw(engine route)) {
		return unless $params{$_};
		$self->$_($params{$_});
	}
    $self->route_positions($self->route->($self->engine));
    my $possible_venue = [];
    $self->SetPossibleVenueWithIndex($possible_venue);

    # If was filled route, then check all route
    unless (scalar @$possible_venue) {
        foreach my $r (grep { $_ ne $self->route } @{$self->routes}){
            #$self->route_positions($r->($self));
            $self->SetPossibleVenueWithIndex($possible_venue);
            last if scalar @$possible_venue;
        }
    }
	$self->venue_idx($possible_venue->[int rand scalar @$possible_venue]->{idx})
		->SetVenue()
		->venue_positions($self->engine->GetVenuePositions($self->venue))
		->SetNextVenue()
		->next_venue_range($self->engine->GetVenueRange($self->next_venue));
	$self->SetTarget();
    my $count_gr = scalar keys %{$self->engine->groups};
    $self->engine->groups->{$count_gr} = $self;
    $self->id($count_gr + 1);

	return $self;
}

sub GetNextVenue {
	my $self = shift;
    my $venue_idx = shift;

    #$self->venue_idx(int rand $self->engine->width * $self->engine->height);
    #if ($self->venue_idx == scalar @{$self->route_positions} - 1) {
    #    $self->venue_idx(0)
    #} else {
    #    $self->venue_idx($self->venue_idx + 1)
    #}
    $self->SetNextRouteVenue($venue_idx);
    $self->next_venue_range($self->engine->GetVenueRange($self->next_venue));
    $self->SetTarget();
    $self->all(0)->inner(0)->time_wait(0);
    return $self;
}


sub SetNextRouteVenue {
	my $self = shift;
    my $venue_idx = shift;

    $self->venue($self->next_venue);
    #if ($self->venue_idx == scalar @{$self->route_positions} - 1) {
    # 	$self->next_venue($self->route_positions->[0]);
    #	return;
    #}
    #$self->next_venue($self->route_positions->[$self->venue_idx + 1]);
    $self->next_venue($self->route_positions->[$venue_idx // int rand $self->engine->width * $self->engine->height]);
}

sub SetTarget {
	my $self = shift;

	$self->target_x(
    	$self->next_venue % $self->engine->width > $self->venue % $self->engine->width
        ? $self->next_venue_range->[1]->[0]
        : $self->next_venue_range->[0]->[0]
    ); 
    $self->target_y(
    	int($self->venue / $self->engine->width) > int($self->venue / $self->engine->width)
        ? $self->next_venue_range->[1]->[1]
        : $self->next_venue_range->[0]->[1]
    );
}

sub SetNextVenue {
	my $self = shift;

    return $self->next_venue(int rand $self->engine->width * $self->engine->height);
	#return $self->next_venue($self->route_positions->[0])
	#	if $self->venue_idx == scalar @{$self->route_positions} - 1;
	#return $self->next_venue(
	#	$self->route_positions->[$self->venue_idx + 1]
	#);
}

sub SetPossibleVenueWithIndex {
	my $self = shift;
	my $possible_venue = shift;

    #warn scalar keys %{$self->engine->venue_free};
	push(@$possible_venue, { idx => $_, venue => $self->route_positions->[$_] })
        for (0..scalar @{$self->route_positions} - 1);
    $possible_venue = [
        grep { exists $self->engine->venue_free->{$_->{venue}} } @$possible_venue
    ];
}

sub SetVenue {
	my $self = shift;

	return $self->venue($self->route_positions->[$self->venue_idx]);
}

sub getCount {return scalar keys %{shift->member} }

sub addMember {
    my ($self, $cell) = @_;
    $self->member->{$cell->id} = $cell;
    return $self;
}

sub delMember {
    my $self = shift;
    return delete $self->member->{shift->id};
}

sub setMaxDensity { shift->max_density(shift) }

sub getMaxDensity { shift->max_density }

sub splitGroup {
    my $self = shift;
    my $new_gr = $self->new(route => $self->route, engine => $self->engine);
    my $c = 0;
    foreach my $cell (values %{$self->member}) {
        if ($self->engine->getVenueHeight() * $self->engine->getVenueWidth() * $cell->max_density <= $self->getCount()) {
            $cell->group($new_gr->id);
            $new_gr->addMember($self->delMember($cell));
        }
        $c++;
    }
    $self->engine->groups->{$new_gr->id} = $new_gr;
}

sub ToHash {
	my $self = shift;

	return { map { $_ => $self->$_ }
				qw(route_positions venue next_venue_range next_venue target_x target_y count_cell)
	};
}

1;