define(['config', './utils'], function (config, utils) {

var form = config.form_customize.get(0);

function validation() {
	var cols = form.cols, max_cols = 100,
		rows = form.rows, max_rows = 100,
		venue_cols_count = form.venue_cols_count,
		venue_rows_count = form.venue_rows_count;

	utils.clearValidationMethod();

	utils.addMethod(utils.checkInteger, [cols]);
	utils.addMethod(utils.checkInteger, [rows]);
	utils.addMethod(utils.checkInteger, [venue_cols_count]);
	utils.addMethod(utils.checkInteger, [venue_rows_count]);

	utils.addMethod(utils.checkMax, [cols, max_cols]);
	utils.addMethod(utils.checkMin, [cols, venue_cols_count.value]);
	utils.addMethod(utils.checkMax, [rows, max_rows]);
	utils.addMethod(utils.checkMin, [rows, venue_rows_count.value]);
	utils.addMethod(utils.checkMax, [venue_cols_count, cols.value]);
	utils.addMethod(utils.checkMax, [venue_rows_count, rows.value]);
	utils.addMethod(utils.checkMultiple, [rows, venue_rows_count.value]);
	utils.addMethod(utils.checkMultiple, [cols, venue_cols_count.value]);

	return utils.applyMethod();
}


return {
		validation: validation,
	}

});