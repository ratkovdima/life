package Life;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Documentation browser under "/perldoc"
  $self->plugin('PODRenderer');
  $self->plugin('Config');

  $self->helper(add => sub { $_[1] + $_[2] });
  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('main#index');  
  $r->websocket('/life')->to('main#life');
}

1;
