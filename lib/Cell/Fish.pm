package Cell::Fish;
use Mojo::Base 'Cell';
use Mojo::Base '-base';

has class_skeleton => 'skeleton';

has action_sex_possible => sub { [] };

has my_food => sub { ['Cell::Plankton'] };

sub new {
    my $Class =  shift;
    my %Params = @_;

    my $self = $Class->SUPER::new(@_);

    $self->need_food(0) unless $self->need_food;

    push @{$self->action_sex_possible}, $self->can($_)
            for qw(_ActionSexRightCell _ActionSexLeftCell
               _ActionSexTopCell _ActionSexDownCell);

    return $self;
}

sub Action {
    my $self =  shift;

    $self->SUPER::Action(@_);

    return if $self->is_die;
    if ($self->need_sex) {
        if ($self->ActionSex()) {
            warn "WAS SEx";
        } else {
            $self->_ActionAlternative();
        }
    } elsif ($self->has_children) {
        if($self->ActionPushChildren()) {
            $self->has_children(0);
            warn "PUSH CHIDREN";
        } else {
            $self->_ActionAlternative();
        }
    } elsif ($self->need_food && $self->need_food > $self->need_food_cycle) {
        if ($self->ActionFood()) {
            warn "WAS FOOD";
        } else {
            $self->ActionMove();
        }
    } else {
        #$self->last_x(е$self->x); $self->last_y($self->y);
        $self->ActionMove();
    }

}

sub _ActionAlternative {
    my $self = shift;

    return 1 if $self->ActionFood();
    return 1 if $self->ActionMove();
    return 0;
}

sub ActionSex {
    my $self = shift;

    foreach (1..scalar @{$self->action_sex_possible}){
        return 1 if $self->action_sex_possible
            ->[int rand scalar @{$self->action_sex_possible}]->($self);
    }
    return 0;
}

sub _ActionSexTopCell {
    my $self = shift;
    
    return 0 if $self->y == 0;
    my $partner = $self->sea->_GetCellObject($self->x, $self->y - 1);
    return $self->AfterSex($partner) if $self->CanSex($partner);
    return 0;
}

sub _ActionSexDownCell {
    my $self = shift;
    
    return 0 if $self->y == $self->sea->rows - 1;
    my $partner = $self->sea->_GetCellObject($self->x, $self->y + 1);
    return $self->AfterSex($partner) if $self->CanSex($partner);
    return 0;
}

sub _ActionSexRightCell {
    my $self = shift;
    
    return 0 if $self->x == $self->sea->cols - 1;
    my $partner = $self->sea->_GetCellObject($self->x + 1, $self->y);
    return $self->AfterSex($partner) if $self->CanSex($partner);
    return 0;
}

sub _ActionSexLeftCell {
    my $self = shift;
    
    return 0 if $self->x == 0;
    my $partner = $self->sea->_GetCellObject($self->x - 1, $self->y);
    return $self->AfterSex($partner) if $self->CanSex($partner);
    return 0;
}

sub CanSex {
    my $self = shift;
    my $partner = shift || return;

    return 1 if ref $self eq ref $partner
        && $self->gender ne $partner->gender
        && $partner->need_sex && !$partner->has_children;
    return 0;
}

sub AfterSex {
    my $self = shift;
    my $partner = shift || return;

    $self->need_sex(0);
    $partner->need_sex(0);
    if ($self->gender eq 'female'){
        $self->has_children(1)
    } else {
        $partner->has_children(1);
    }
    return 1;
}

1;
